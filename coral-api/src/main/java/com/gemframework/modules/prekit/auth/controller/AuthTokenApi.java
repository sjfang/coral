/**
 * 开源版本请务必保留此注释头信息，若删除捷码开源〔GEMOS〕官方保留所有法律责任追究！
 * 本软件受国家版权局知识产权以及国家计算机软件著作权保护（登记号：2018SR503328）
 * 不得恶意分享产品源代码、二次转售等，违者必究。
 * Copyright (c) 2020 gemframework all rights reserved.
 * http://www.gemframework.com
 * 版权所有，侵权必究！
 */
package com.gemframework.modules.prekit.auth.controller;
import com.gemframework.common.annotation.ApiLog;
import com.gemframework.common.utils.GemValidateUtils;
import com.gemframework.model.common.BaseResultData;
import com.gemframework.model.response.AccessTokenResponse;
import com.gemframework.model.response.RefreshTokenResponse;
import com.gemframework.modules.prekit.auth.entity.request.AccountPasswordAuthRequest;
import com.gemframework.modules.prekit.auth.entity.request.RefreshAccessTokenRequest;
import com.gemframework.service.MemberService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

@RestController
public class AuthTokenApi {

	@Resource
	private MemberService memberService;


	@ApiLog(name = "获取token")
	@GetMapping("getAccessToken")
//	@ApiSign
	public BaseResultData getAccessTokenByAccount(@RequestBody AccountPasswordAuthRequest request) {
		//参数校验器
		GemValidateUtils.GemValidate(request);
		AccessTokenResponse data = memberService.getAccessTokenByAccount(request.getAccount(),request.getPassword());
		return BaseResultData.SUCCESS(data);
	}

	@ApiLog(name = "刷新token")
	@GetMapping("refreshAccessToken")
//	@ApiSign
	public BaseResultData refreshAccessToken(@RequestBody RefreshAccessTokenRequest request) {
		//参数校验器
		GemValidateUtils.GemValidate(request);
		RefreshTokenResponse data = memberService.refreshAccessToken(request.getMemberId(),request.getRefreshToken());
		return BaseResultData.SUCCESS(data);
	}

}

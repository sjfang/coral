/**
 * 开源版本请务必保留此注释头信息，若删除捷码开源〔GEMOS〕官方保留所有法律责任追究！
 * 本软件受国家版权局知识产权以及国家计算机软件著作权保护（登记号：2018SR503328）
 * 不得恶意分享产品源代码、二次转售等，违者必究。
 * Copyright (c) 2020 gemframework all rights reserved.
 * http://www.gemframework.com
 * 版权所有，侵权必究！
 */
package com.gemframework.common.utils.security;

import com.gemframework.model.common.SaltPassword;

public class GemPasswordUtils {

    // 加密次数
    public static final int INTERATIONS = 2;

    // 盐size
    private static final int SALT_SIZE = 8;
    /**
     * @Title: validatePassword
     * @Description: 验证密码
     * @author zhangww
     * @param inputPassword
     *            客户端传过来的密码
     * @param password
     *            密文密码
     * @param salt
     *            盐值
     * @return
     */
    public static boolean validatePassword(String inputPassword, String password, String salt) {
        byte[] hashPassword = GemDigestsUtils.sha1(inputPassword.getBytes(),
                GemEncodesUtils.decodeHex(salt), INTERATIONS);
        String encryptPassword = GemEncodesUtils.encodeHex(hashPassword);
        return password.equals(encryptPassword);
    }

    public static SaltPassword saltPassword(String password){
        byte[] saltArr = GemDigestsUtils.generateSalt(SALT_SIZE);
        String salt = GemEncodesUtils.encodeHex(saltArr);
        byte[] hashPassword = GemDigestsUtils.sha1(password.getBytes(), saltArr, INTERATIONS);
        String pass = GemEncodesUtils.encodeHex(hashPassword);
        SaltPassword saltPassword = SaltPassword.builder()
                .password(pass)
                .salt(salt)
                .build();
        return saltPassword;
    }
}